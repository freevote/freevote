# README #

FreeVote is an open source electronic voting application designed to run on low-cost hardware such as the [Raspberry Pi](https://raspberrypi.org). Our not so modest goal is to make expensive, proprietary, non-transparent, and horribly insecure electronic voting machines a thing of the past. Because [stuff](http://arstechnica.com/tech-policy/2015/04/meet-the-e-voting-machine-so-easy-to-hack-it-will-take-your-breath-away/) [like](http://arstechnica.com/information-technology/2009/04/voting-machine-expert-criticizes-clueless-industry-report/) [this](http://arstechnica.com/security/2009/08/researchers-demonstrate-real-world-voting-machine-attack/) [and](http://arstechnica.com/tech-policy/2007/08/california-to-recertify-insecure-voting-machines/) [this](http://www.hackingdemocracy.com/) [and](http://thehill.com/policy/cybersecurity/222470-states-ditch-electronic-voting-machines) [this](http://betanews.com/2015/04/17/us-electronic-voting-machines-incredibly-easy-to-hack/).

I could go on, but you get the idea. The mechanism by which we exercise our right to vote is corrupt and broken. And we want to fix it.

## What's In This Repository ##

This repository contains the code, issue tracker, and wiki for the FreeVote project.

If you'd like to get involved, please contact us!

## Running FreeVote ##

It's easy! (But if you need help or run into issues, please let us know.)

### Prerequisites ###

Make sure you have the following on your machine before proceeding.

1. [Git](http://git-scm.com/)
2. [VirtualBox](https://virtualbox.org)
3. [Vagrant](https://vagrantup.com)

### Installation ###

1. git clone the project to your machine
2. cd into the project directory and run `vagrant up`
3. `vagrant ssh`
4. `cd /vagrant/freevote`
5. `python3 manage.py runserver 0.0.0.0:8000`
6. Browse to http://localhost:8000 on your host machine

### A Bit More Information ###

FreeVote currently uses an Ubuntu 14.04 (Trusty Tahr) Vagrant box for running a development instance of the project. This is why you need to install VirtualBox and Vagrant in order to run FreeVote. Eventually we'll get a Raspberry Pi image going, but we're most interested in the simplest way possible for people to get the project running.

When you clone the project you'll have everything you need to get going, and beyond VirtualBox and Vagrant you don't need to install anything else on your host machine. The provisioning script for Vagrant will install all the necessary packages and applications, so a simple `vagrant up` will (if we've done our job right) launch and configure the server.

The only additional step required, because this is a Django project, is to ssh into the Vagrant box and start the Django development server. Or of course if you're familiar with Django you can use your favorite IDE or whatever tools you like to make this step a bit more convenient. (We may eventually start the Django process automatically when you start the Vagrant box and/or include a more production-level web server, but again, we're starting simple.)

Once the Vagrant box is up, the port forwarding is configured such that you can hit the app and the CouchDB administrator from your host machine's browser:
* http://localhost:8000 (the FreeVote app)
* http://localhost:5984/_utils (the CouchDB administrator)

### Technologies Used ###

FreeVote is built largely on the following technologies:

* [Python](https://python.org)
* [Django](https://djangoproject.com)
* [CouchDB](http://couchdb.apache.org)

And of course the usual smattering of stuff used in modern webapps such as [Twitter Bootstrap](http://getbootstrap.com/), [jQuery](http://jquery.com/), and the other usual suspects.

## Security ##

Please be aware that out of the box FreeVote in its current incarnation **is not secure**. Not in any way, shape, or form.

Yes, security is important. Hugely important. And it's something we are absolutely going to address in a massively comprehensive way when the time comes.

For now we're interested in getting a voting application working, and working well. Once we have that side of things in good shape we will secure the heck out of FreeVote in ways that will put the current electronic voting efforts to shame. Wait. That's not even a challenge to be more secure than the current voting machines. So let's say our goal is to secure FreeVote to the point where it's held up as a model of security.

## How Can I Help? ##

If you're interested in this effort in any capacity we'd love to hear from you. Coding, writing tests, doing design, evangelizing, we need it all. So if the thought of upending the current horrid state of affairs with electronic voting machines gets you excited, please get in touch.