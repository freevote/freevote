from django.conf.urls import include, url
from django.contrib import admin

from freevote import views


urlpatterns = [
    # Examples:
    # url(r'^$', 'freevote.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', views.home, name='home')
]
